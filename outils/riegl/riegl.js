let radius = 500;
let width = 1080;
let radius_int = 70;

let mode = "vizu1";

if (mode == "vizu3"){
    width=400;
    radius_int = 10;
    radius = 200;
}

let margin_val = 0.05;

let vizu1_state = -7;

let root;
let udpate_attr;
let sel_pos;
let sel_top;
let neighbour;

let panel = d3.select("#panel");
console.log('panel', panel);

/**
 * Met le texte sur plusieurs lignes s'il comporte plusieurs mots
 *
 * @param txt - le texte a convertir
 */
function textWithLineBreaks(txt) {
		s = txt.split(' ');
		if (s.length > 1) {
			  var txt_out = '';
			  var lst_lines = [];
			  var was_short = false;
			  for (var i in s) {
				    if (!was_short) {
					      lst_lines.push(s[i]);
					      if (s[i].length < 4) {
						        was_short = true;
					      }
				    } else {
					      lst_lines[lst_lines.length - 1] += ' ' + s[i];
					      was_short = false;
				    }
			  }
			  for (var i in lst_lines) {
				    if (i == 0) {
					      txt_out += "<tspan x='0' dy='-0.2em'>" + lst_lines[i] + "</tspan>";
				    } else {
					      txt_out += "<tspan x='0' dy='1.1em'>" + lst_lines[i] + "</tspan>";
				    }
			  }
			  return txt_out;
		} else {
			  return txt;
		}
}

function map_range(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}

let lerp_radius = d3.interpolate(radius_int, radius);

function y_from_state(depth, state){
    let y0 = 1;
    let y1 = 1;
    if (depth <= state + 1){
        y0 = (depth-1)/(state+1);
        y1 = (depth)/(state+1);
    }
    return [y0, y1];
}

function y_from_state_vizu2(depth, state){
    if (state < 2){
        return y_from_state(depth, state);
    }else{
        if (depth < 3){
            return [(depth-1)/6, depth/6];
        }else{
            if (state == 2)
            {
                if (depth == 3){
                    return [2/6, 1];
                }else{
                    return [1, 1];
                }
            }else{
                if (depth == 3){
                    return [2/6, 3/6];
                }else{
                    return [3/6, 1];
                }
            }
        }
    }
    return [1, 1];
}

/**
 * Renvoie les angles pour le niveau 2
 *
 * @param {integer} pos - position du niveau 1 entre 0 et 5
 * @param {object} p - objet pour lequel on veut récupérer les angles
 * @return {[float, float]} Un tableau contenant les angles de début et de fin de l'arc
 */

function x_from_pos(pos, p){
    let x0, x1;
    if (pos < p.pos){
        console.log(p.target.x0);
        x0 = p.target.x0 - (p.pos-pos) * Math.PI/5;
    }else{
        x0 = p.target.x1 + (pos-p.pos-1) * Math.PI/5;
    }

    if (pos == 0 || pos == 3) x0 += margin_val;

    if (pos == 5 || pos == 2) x1 -= margin_val;

    if (pos == 1 || pos == 4){
        x1 = x0 + Math.PI/5;
    }else{
        x1 = x0 + Math.PI/5 - margin_val;
    }

    if (sel_pos == 0 && pos == 5){
        x1 -= margin_val;
    }else if(sel_pos == 2 && pos == 3){
        x0 += margin_val;
    }else if(sel_pos == 2 && pos == 2){
        x1 -= margin_val;
    }else if(sel_pos == 5 && pos == 0){
        x0 += margin_val;
    }


    return [x0, x1];
}

/**
 * Renvoi les données sous forme hierarchique en
 * renseignant les coordonnées des arcs à l'état initial
 *
 * rayon intérieur et extérieur à 1 sauf pour le niveau 1 (valeurs)
 * angles correspondant à l'état final de VIZU1
 */

let partition = data => {
    console.log("data", data);
    root = d3.hierarchy(data);
    console.log("hierarchy", root);
    root.sum(function(d){
        if (d.children != undefined){
            return 0;
        }else{
            return 10;
        }
    });

    let part = d3.partition()
        .size([2 * Math.PI, root.height + 1])
    (root);
    console.log("partition", part);

    let angle_d1 = Math.PI/3;

    let angle_d2 = Math.PI/6;
    let max_d2 = 2;

    let max_d3 = 5;  // TODO: get from data
    let angle_d3 = angle_d2/max_d3;

    let max_d4 = 7;  // TODO: get from data
    let angle_d4 = angle_d3/max_d4;

    let cur_x = 0;
    part.y0 = 0;
    part.y1 = 0;
    let children_pos = 0;
    part.children.forEach(function(d){
        let curcur_x = cur_x;
        d.pos = children_pos;
        d.children.forEach(function(c){
            c.pos = children_pos; 
            let start_x0 = curcur_x + (angle_d2 - c.children.length*angle_d3)/2;
            c.children.forEach(function(e){
                e.pos = children_pos;
                let start_x0_d4 = start_x0 + (angle_d3 - e.children.length*angle_d4)/2;
                e.children.forEach(function(f){
                    f.pos = children_pos;
                    f.x0 = start_x0_d4;
                    start_x0_d4 += angle_d4;
                    f.x1 = start_x0_d4;

                    f.y0 = 1;
                    f.y1 = 1;
                });

                e.x0 = start_x0;
                start_x0 += angle_d3;
                e.x1 = start_x0;

                e.y0 = 1;
                e.y1 = 1;
            });

            c.x0 = curcur_x;
            curcur_x += angle_d2;
            c.x1 = curcur_x;

            c.y0 = 1;
            c.y1 = 1;
        });

        children_pos += 1;

        d.init_x0 = cur_x;
        cur_x +=  angle_d1;
        d.init_x1 = cur_x ;

        d.y0 = 0;
        d.init_y1 = 1;

        d.y1 = 0;

        d.x0 = (d.init_x0 + d.init_x1)/2;
        d.x1 = d.x0;

        console.log(d);
    });

    // part.children.forEach(function(d){
    //     if (d.pos == 0 || d.pos == 3){
    //         d.init_x0 = d.init_x0 + 0.01;
    //     }else if (d.pos == 2 || d.pos == 5){
    //         d.init_x1 = d.init_x1 - 0.01;
    //     }
    // });

    return part;
};

/**
 * Renvoie une fonction pour créer l'arc svg (chemin)
 * en fonction des coordonnées de l'objet
 *
 * x0: angle de départ (en radians)
 * x1: angle d'arrivée
 * y0: rayon intérieur (entre 0 et 1)
 * y1: rayon extérieur
 */

let arc = d3.arc()
    .startAngle(d => d.x0)
    .endAngle(d => d.x1)
    .padAngle(function(d) {
        if ( d.depth == 4) {
            return 0.001;
        }else{
            return Math.min((d.x1 - d.x0) / 2, 0.005);
        }
    })
    .padRadius(radius * 1.5)
    .innerRadius(d => lerp_radius(d.y0))
    .outerRadius(d => lerp_radius(d.y1)-3);

/**
 * Renvoie les infos à afficher dans le panneau
 *
 * @param {object} d - l'objet survolé
 * @return {string} Le html à afficher
 */

function create_infos(d){
    let txt = "";
    d.ancestors()
        .forEach(a => {
            txt = a.data.label + " > " + txt;
        });
    txt = txt.slice(0, txt.length-2);
    txt = txt + "<h2>" + d.data.label + "</h2>" + d.data.définition;

    if (d.depth == 4){
        let neighbors = "";
        txt = txt + "<h3> Autres attributs </h3>\n";
        txt = txt + "<ul>\n";
        d.parent.children.forEach(c => {
            if (c.data.label != d.data.label){
                neighbors += "<li>" +c.data.label + "</li>\n";
            }
        });
        txt = txt +  neighbors ;
        txt = txt + "</ul>\n";

    }

    if (d.depth > 1){
        let par = d.parent;
        while (par.depth != 1){
            par = par.parent;
        }
        txt += "<h3> Valeur : " + par.data.label +"</h3>";
        txt += par.data.définition;
    }

    return txt;
}

// d3.json("riegl.json").then(function(data){


function load_data(data){
    let color_sc = d3.scaleOrdinal(d3.quantize(d3.interpolateRainbow, data.children.length + 1));

    function color(d){
        while (!d.data.color && d.depth > 1){
            d = d.parent;
        }

        if (d.data.color){
            return d.data.color;
        }else{
            return color_sc(d.data.label);
        }
        return undefined;
    }

    const root = partition(data);
    console.log("partition", root);
    root.each(d => d.current = d);

    // donne un identifiant temporaire le temps de faire un test
    let cpt = 0;
    root.leaves().forEach(d => {d.data.id = "l"+cpt; cpt+=1;});

    const svg = d3.select("#vis")
          .style("width", "100%")
          .style("height", "auto")
          .style("font", "10px sans-serif");

    if (mode != "vizu3"){

        const but = svg.append("g").attr("id", "mode-button");
        
        const but_back = but.append("rect")
              .attr("width", 150)
              .attr("height", 40)
              .attr("x", 10)
              .attr("y", 10)
              .attr("fill", "lightgray")
              .attr("stroke-width", "4px")
              .attr("stroke", "lightgray")
              .text("test")
              .text("mode");

        const but_text = but.append("text")
              .attr("x", 10+150/2)
              .attr("y", 35)
              .attr("text-align","center")
              .text("Présentation");

        but.on("click", function(d){
            if (mode == "vizu1"){
                mode = "vizu2";
                but_back.style("fill", "#444");
                but_text.text("Exploration");
                but_text.style("fill", "white");
            }else{
                mode = "vizu1";
                but_text.text("Présentation");
                but_text.style("fill", "black");
                but_back.style("fill", "lightgray");
            }
            init();
            update();
        });
    }

    const g = svg.append("g")
          .attr("transform", `translate(${width / 2},${width / 2})`)
          .append("g")
          .attr("transform", "rotate(-90)");

    const path = g.append("g")
          .selectAll("path")
          .data(root.descendants())
          .join("path")
          .attr("fill", color)
          .attr("fill-opacity", d => arcVisible(d.current) ? 1 : 0)
          .attr("d", d => arc(d.current));

    if (mode != "vizu3"){
        path//.filter(d => d.children)
            .style("cursor", "pointer")
            .on("click", clicked)
            .on("mouseover", function(d){
                if (mode == "vizu1")
                {
                    root.descendants().forEach(e => {e.emph = false;});
                    d.emph = true;
                    d.ancestors().forEach(e => { e.emph = true;});

                    path.transition(100)
                        .filter(e => e.target.visible)
                        .attr("fill-opacity", e => e.emph ? 1 : 0.5);
                }

                d3.select("#panel").html(create_infos(d));
            })
            .on("mouseleave", function(d){
                if (mode=="vizu1"){
                    d.emph = false;
                    d.ancestors().forEach(e => { e.emph = false;});

                    path.transition(100).filter(e => e.target.visible)
                        .attr("fill-opacity", 1);
                }

                d3.select("#panel").html("");
            });

        svg.on("click", function(){
            if (vizu1_state < 0){
                vizu1_state += 1;
                console.log("initial anim: ", vizu1_state);
                if (vizu1_state == 0){
                    init();
                    update();
                }else{
                    clicked_vizu0();
                }
            }
        });

    }

    const label = g.append("g")
          .attr("pointer-events", "none")
          .attr("text-anchor", "middle")
          .style("user-select", "none")
          .selectAll("text")
          .data(root.descendants().slice(1))
          .join("text")
          .attr("dy", "0.35em")
          .attr("fill-opacity", d => +labelVisible(d.current))
          .attr("font-size", fontSize)
          .attr("transform", d => labelTransform(d.current))
          .html(d => d.depth < 4 ? textWithLineBreaks(d.data.label) : d.data.label);

    const parent = g.append("circle")
          .datum(root)
          .attr("r", radius_int)
          .attr("fill", "none")
          .style("cursor", "pointer")
          .attr("pointer-events", "all")
          .on("click", clicked);

   
    if (mode == "vizu3"){
        init();
        vizu3();
         update();
    }

    function update(){

        const t = g.transition().duration(750);

        // Transition the data on all arcs, even the ones that aren’t visible,
        // so that if this transition is interrupted, entering arcs will start
        // the next transition from the desired position.
        const trans = path.transition(t)
            .tween("data", d => {
                const i = d3.interpolate(d.current, d.target);
                return t => d.current = i(t);
            });

        trans.filter(function(d) {
                return +this.getAttribute("fill-opacity") || arcVisible(d.target);
            })
            .attr("fill-opacity", d => arcVisible(d.target) ? (d.children ? 1 : 1) : 0)
            .attrTween("d", d => () => arc(d.current));

        if (mode == "vizu3"){
            trans.attr("fill", function(d){
                    if (!d.sel){
                        return "gray";
                    }else{
                        return color(d);
                    }
                });
        }else{
            trans.attr("fill", color);
        }

        label.filter(function(d) {
            return +this.getAttribute("fill-opacity") || labelVisible(d.target);
        }).transition(t)
            .attr("fill-opacity", d => +labelVisible(d.target))
            .attr("font-size", fontSize)
            .attrTween("transform", d => () => labelTransform(d.current));

    }

    function clicked_vizu0(){
        console.log('vizu0');
        if (vizu1_state == -6){
            root.descendants().forEach(function(d){
                d.target = {
                    pos: d.pos,
                    depth: d.depth,
                    x0: d.x0,
                    x1: d.x1,
                    y0: d.y0,
                    y1: d.y1
                };
            });
        }
        if (vizu1_state < -3){

            root.children.forEach(function(d){
                if (d.pos == vizu1_state + 7 || d.pos == (vizu1_state + 10)%6 ){
                    d.target.x0 = d.x0 -0.01;
                    d.target.x1 = d.x1 + 0.01;
                    d.target.y1 = 1;
                }
            });
        }else{
            root.children.forEach(function(d){
                if (d.pos == vizu1_state + 4 || d.pos == (vizu1_state + 7)%6 ){
                    d.target.x0 = d.x0 - Math.PI/6;
                    d.target.x1 = d.x1 + Math.PI/6;
                    d.target.y1 = 1;
                }
            });
        }

    update();
    }

    function clicked_vizu1(p){
        if (p.depth > 0){
            vizu1_state = Math.min(vizu1_state+1, 3);
        }else{
            vizu1_state = Math.max(vizu1_state-1, 0);
        }

        root.each(function(d){
            let n_y0, n_y1;

            [n_y0, n_y1] = y_from_state(d.depth, vizu1_state);

            console.log("ny", n_y0, n_y1);
            d.target = {
                pos: d.pos,
                depth: d.depth,
                x0: d.depth == 1 ? d.init_x0 : d.x0,
                x1: d.depth == 1 ? d.init_x1 : d.x1,
                y0: n_y0,
                y1: n_y1,
                visible: (d.depth > 0 && d.depth <= vizu1_state + 1)
            };
        });
    }

    function init(){
        vizu1_state = 0;
        root.each(function(d){
            if (d.depth == 1){
                d.x0 = d.init_x0;
                d.x1 = d.init_x1;
                d.y1 = d.init_y1;
            }
            d.target = {
                depth: d.depth,
                x0: d.x0,
                x1: d.x1,
                y0: d.y0,
                y1: d.y1,
                visible: d.depth == 1
            };
        });
        sel_pos = undefined;
    }

    function vizu2_depth1(p){
        console.log('vizu2_depth1');

        vizu1_state = p.depth + 2; // use for visibility
        sel_pos = p.pos;           //
        sel_top = p.pos < 3;

        const x0 = sel_top ? 0 : Math.PI;
        p.target = {
            depth: p.depth,
            x0: x0,
            x1: x0 + Math.PI
        };

        // if (p.pos == 0 || p.pos == 3){
        //     p.target.x0 += margin_val;
        // }

        // if (p.pos == 2 || p.pos == 5){
        //     p.target.x1 -= margin_val;
        // }


        [ p.target.y0, p.target.y1 ] = y_from_state_vizu2(p.depth, p.depth);

        root.each(function(d){
            if (d != p){
                d.target = {
                    pos: d.pos,
                    depth: d.depth,
                    x0: d.x0,
                    x1: d.x1,
                    y0: d.y0,
                    y1: d.y1
                };

                if ( d.depth == p.depth ){
                    [d.target.x0, d.target.x1] = x_from_pos(d.pos, p);
                }else if(d.depth > 0){
                    let pp = d.parent;
                    d.target.x0 = map_range(d.x0, pp.x0, pp.x1, pp.target.x0, pp.target.x1);
                    d.target.x1 = map_range(d.x1, pp.x0, pp.x1, pp.target.x0, pp.target.x1);
                    if (d.pos == sel_pos) [d.target.y0, d.target.y1] = y_from_state_vizu2(d.depth, p.depth);
                }
            }
        });

        root.descendants()
            .forEach(d => d.target.visible = d.depth == 1);
        p.children.forEach(d => {d.target.visible = true;console.log(d.target);});

    }

    function vizu2_depth2(p){
        if (p.target.x0%Math.PI == 0){
            p.target.x1 = p.target.x0 + 3*Math.PI/4;
            neighbour = p.parent.children[1];
            neighbour.target.x0 = p.target.x1;
        }else{
            p.target.x0 = p.target.x1 - 3*Math.PI/4;
            neighbour = p.parent.children[0];
            neighbour.target.x1 = p.target.x0;
        }


        root.descendants()
            .filter(d => d.depth > 2)
            .forEach(function(d){
                let pp = d.parent;
                d.target.x0 = map_range(d.x0, pp.x0, pp.x1, pp.target.x0, pp.target.x1);
                d.target.x1 = map_range(d.x1, pp.x0, pp.x1, pp.target.x0, pp.target.x1);
                d.target.y1 = 1;
                d.target.y0 = 1;
                d.target.visible = false;
            });

        root.descendants()
            .filter(d => d.pos == sel_pos)
            .forEach(function(d){
                if ( d != neighbour){
                    [d.target.y0, d.target.y1] = y_from_state_vizu2(d.depth, p.depth);
                }else{
                    d.target.y0 = y_from_state_vizu2(d.depth, p.depth)[0];
                    d.target.y1 = 1;
                }
            });

        console.log(p.children);
        p.children.forEach(d => {d.target.visible = true;console.log(d.target);});
    }

    function vizu2_depth3(p){

        if (!p.children[0].target.visible){

            p.ancestors()
                .filter(d => d.pos == sel_pos)
                .forEach(function(d){
                    [d.target.y0, d.target.y1] = y_from_state_vizu2(d.depth, p.depth);
                });

            p.parent.children.forEach(d => d.target.y0 = y_from_state_vizu2(d.depth, p.depth)[0]);
            p.descendants().forEach(d => [d.target.y0, d.target.y1] = y_from_state_vizu2(d.depth, p.depth));
            neighbour.target.y0 = y_from_state_vizu2(neighbour.depth, p.depth)[0];

            p.children.forEach(d => d.target.visible = true);
        }else{
            p.target.y1 = 1;
            p.children.forEach(d => {d.target.y0 = 1; d.target.y1 = 1;});
            p.children.forEach(d => d.target.visible = false);
        }
    }


    function update_vizu3(n){
        console.log("update vizu3");
        root.descendants().forEach(d => {d.sel = false;});
        root.leaves().forEach(d => {
            d.sel = (n.includes(d.data.id));
            if (d.sel){
                d.ancestors().forEach(d => {d.sel = true;});
            }
        });

        vizu3();
        update();
    }

    update_attr = update_vizu3;

    function vizu3(){
        root.descendants().forEach(function(d){
            let y1 = d.depth/6;
            if (d.sel && d.depth==4 ) {
                y1 =1;
                console.log("ok");
            }
            d.target = {
                depth: d.depth,
                pos: d.pos,
                x0: d.x0,
                x1: d.x1,
                y0: (d.depth-1)/6,
                y1: y1
            };
        });
    }

    function clicked_vizu2(p){
        if (p.depth == 0){
            init();
        }else if ( p.depth == 1 ){
            vizu2_depth1(p);
        }else if (p.depth == 2){
            vizu2_depth2(p);
        }else if (p.depth == 3){
            vizu2_depth3(p);
        }
    }

    function clicked(p) {
        console.log('clicked', p.target.visible);
        if (p.target.visible || p.depth == 0)
        {
            if (vizu1_state >= 0){
                if ( mode == "vizu1" ){
                    clicked_vizu1(p);
                }else if (mode == "vizu2"){
                    clicked_vizu2(p);
                }
                update();
            }
        }
    }

    function arcVisible(d) {
        if ( mode == "vizu1" || mode != "vizu3" && sel_pos == undefined ){
            if (vizu1_state < 0){
                return d.depth == 1;
            }
            return d.visible;
        }else if (mode == "vizu2"){
            return d.visible;
        }else if (mode == "vizu3"){
            let vis = d.depth == 0 ? false : true;
            return vis;
        }
        return true;
    }

    function labelVisible(d) {
        if ( mode == "vizu1" || mode!= "vizu3" && sel_pos == undefined)
        {
            return d.depth > 0 && d.depth <= vizu1_state +1 && d.depth < 4;
        }else if ( mode == "vizu2"){
            return d.depth > 0 &&  (d.pos == sel_pos && d.depth < vizu1_state) || d.depth == 1 || d.visible;
        }
        return false;
    }

    function labelTransform(d) {
        let radial = true;
        if (mode == "vizu1" && d.depth < 3){
            radial = false;
        }

        if (mode == "vizu2" && d.depth < 4){
            radial = false;
        }

        const x = (d.x0 + d.x1) / 2 * 180 / Math.PI;
        const y = lerp_radius((d.y0 + d.y1) / 2);
        let rot = 0;
        if (x > 270 | x < 90){
            rot = 180;
        }

        // let dec = 0
        // if (d.label.includes(" ")){
        //     dec = -5;
        // }
        
        if (radial){
            return `rotate(${x - 90}) translate(${y},0) rotate(${rot})`;
        }else{
            return `rotate(${x - 90}) translate(${y-10},0) rotate(${180-x})`;
        }
    }

    function fontSize(d){
        let font_size = 14;
        if (mode == "vizu2" && d.depth < 3){
            font_size = 25;
        }
        
        if (mode == "vizu1"){
            font_size = 12;
            if (d.depth < 3){
                font_size = 20;
            }
            if (vizu1_state > 2 && d.depth > 1){
                font_size = 12;
                if (d.depth == 3) font_size = 10;
            }
        }

        return font_size;
    }

    return svg.node();
};


function create_riegl(data_file){
    d3.json(data_file).then(data => load_data(data));
}
