//crée le menu dans la div menu, var deroulant est l'arborescence du site

$(document).ready(function () {
    var is_accueil = $('body').hasClass('accueil');
    var is_rang1 = $('body').hasClass('rang1');

    console.log(is_accueil);
    var deroulant = [{
                    'titre': 'Présentation',
                    'lien': '/index.html',
                    'class': 'menu',
                    'children':[{
                        'titre': 'Présentation',
                        'lien': '/index.html',
                        'class': '#presentation',
                    }]
        },
        {
            'titre': 'Outils',
            'class': 'menu',
            'children': [{
                     
                    'titre': 'Arborescence',
                    'lien': '/territoires/presentation.html',
                    'class': 'sousmenu',

		},
                {
                    'titre': 'Modèle patrimonial',
                    'lien': '/territoires/presentation.html#RIEGL',
                    'class': 'sousmenu',
        },
                {
                    'titre': 'Bibliographie',
                    'lien': '/territoires/presentation.html#BIBLIO',
                    'class': 'sousmenu',
		}]
	}, {
            'titre': 'Territoires',
            'class': 'menu',
            'children': [{
                     
                    'titre': 'Urbino',
                    'lien' : '/territoires/urbino.html',
                    'class': 'sous-menu-déroulant',
                    'children': [{
                        
                        'titre': '2009',
                        'class': 'sousmenu',
                        'lien': '/territoires/2009/index.html'
    }, {
                        'titre': '2010',
                        'class': 'sousmenu',
                        'lien': '/territoires/2010/index.html'
    }, {
                        'titre': '2011',
                        'class': 'sousmenu',
                        'lien': '/territoires/2011/index.html',

    }],

    }, {
                    'titre': 'Lisbonne',
                    'lien' : '/territoires/lisbonne.html',
                    'class': 'sous-menu-déroulant',
                    'children': [{
                        
                        'titre': '2012',
                        'class': 'sousmenu',
                        'lien': '/territoires/2012/index.html',
                        
    }, {
                        'titre': '2013',
                        'class': 'sousmenu',
                        'lien': '/territoires/2013/index.html',
                       
    }, {
                        'titre': '2014',
                        'class': 'sous-sous-menu',
                        'lien': '/territoires/2014/index.html',
                       
    }, {
                        'titre': '2015',
                        'class': 'sous-sous-menu',
                        'lien': '/territoires/2015/index.html',
    }],
                },
                { 
                    'titre': 'Athènes',
                    'lien': '/territoires/athenes.html',
                    'class': 'sous-menu-déroulant',
                    'children': [{
                        
                        'titre': '2016',
                        'class': 'sous-sous-menu',
                        'lien': '/territoires/2016/index.html',
                        
    }, {
                        'titre': '2017',
                        'class': 'sous-sous-menu',
                        'lien': '/territoires/2017/index.html',
                        
    }, {
                        'titre': '2018',
                        'class': 'sous-sous-menu',
                        'lien': '/territoires/2018/index.html',
                           }]

                },{
                    'titre': 'Naples',
                    'lien': '/territoires/naples.html',
                    'class': 'sous-menu-déroulant',
                    'children': [{
                        
                        'titre': '2019',
                        'class': 'sous-sous-menu',
                        'lien': '/territoires/2019/index.html',
                        
    }, {
                        'titre': '2020',
                        'class': 'sous-sous-menu',
                        'lien': '/territoires/2020/index.html',
                            }]
                }]
        }];


    deroulant.forEach(function (e) { //permet de changer les liens selon index ou pas

//Niveau 0 du menu

        var nav = $('<li class="toggleSubMenu"><span>' + e['titre'] + '</span></li>').appendTo($('#navig'));
        console.log(nav);

 /*   for (var h in e.children) { //pour distinguer présentation
        var b = e.children[h];
        if (!b.children) { //si il existe pas de sousmenu on a présentation
            nav.append('<ul><a class="sous" href="#Presentation' + rep + b['lien'] +'">' + c['titre'] + '</a></ul>');
        } else {
            */
            
//Niveau 1 du menu
        var nav_ul = $('<ul class="subMenu"> </ul>').appendTo(nav);
        for (var i in e.children) { //dans les enfants de menu
            var c = e.children[i]; //c prend la valeur des sousmenu
            if (!c.children) { // si il existe pas de sous-sous-menu
                nav_ul.append('<li><a class="sous" href="' + c['lien'] + '">' + c['titre'] + '</a></li>');
            } else { // sinon
//Niveau 2 du menu
                var nav2 = $('<li class="toggleSubSubMenu"><a class="sous" href="' + c['lien'] + '">' + c['titre'] + '</a></li>').appendTo(nav_ul);
                var nav_ul2 = $('<ul class="sousmenu"> </ul>').appendTo(nav2);
                for (var j in c.children) {
                    var f = c.children[j]
                    var nav3 = $('<li><a class="sousous" href="' + f['lien'] + '">' + f['titre'] + '</a></li>').appendTo(nav_ul2);

                }
            }
        }

    });

	
	
//crée le titre de la page dans la div head avec une fonction if qui permet de changer les liens selon index ou pas

    console.log(deroulant);

var header = $('<a class="menu_btn"><img src="/outils/menu/icones/menu-ouvre.png">  Menu</a><strong><a class="entete" href="' + '/index.html"> | Singularités des territoires </a></strong><a class="entete" href ="http://morpholab.fr/fr/home/"> | MorphoLab</a><a class="entete" href ="http://www.nantes.archi.fr/"> | Ensa Nantes</a>').appendTo($('#head'));	

var headerOpen = $('<div class="header1"><a class="menu_btn"><img src="/outils/menu/icones/menu-ferme.png">  Menu</a><strong><a class="entete" href="' + '/index.html"> | Singularités des territoires </a></strong><a class="entete" href ="http://morpholab.fr/fr/home/"> | MorphoLab</a><a class="entete" href ="http://www.nantes.archi.fr/"> | Ensa Nantes</a></div>').appendTo($('#menu'));	
	
//crée le footer dans la div footer avec une fonction if qui permet de changer les liens selon index ou pas 

/*
    var footer = $('<table><tr><td><a class="floatleft" href="https://www.facebook.com/groups/345637762171186/"><img src="' + repe + 'images/footer/fb.png"></a><br><a class="floatleft" href="https://twitter.com/morpho_lab"><img src="' + repe + 'images/footer/twitter.png"></a></td><td class="floatright">©SingularitésDesTerritoires2018</td></tr></table>').appendTo($('#footer'))
*/

    
    //Affichage du block menu
    
    $(".close").css("display", "none");

    var isMenuOpen = false;
    var isNavOpen = true;
    var isNavFull = false;

    $('.menu_btn').click(function () {
    
        if (isMenuOpen == false) {
            document.getElementById("menu").style.display = "block";
            isMenuOpen = true;
        } else {
            document.getElementById("menu").style.display = "none";
            isMenuOpen = false;
        }

    });
    
});
