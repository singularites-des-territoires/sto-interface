// Fichier Torus.js obsolète

var width = 1400;
var height = 1400;

    
var svg = d3.select(document.getElementById("tore")).append("svg")
    .attr("width", width)
    .attr("height", height)
    .style("overflow", "visible");

d3.json("/outils/combi/TestCoef/CombiGold.json").then(function (data) {

console.log(data);

    data.nodes.forEach(d=>{
        d.links = [];
        d.connected = [];
    });

var x = d3.scaleLinear()
    .domain([0, data.nodes.length])
    .range([0, width]);

var y = d3.scaleLinear()
    .domain([0, data.nodes.length])
    .range([0, height]);

    var links_g = svg.append("g").attr("class", "links");
    var nodes_g = svg.append("g").attr("class", "nodes");

var ptRiegl = nodes_g.selectAll("circle")
    .data(data.nodes.map(function (d) { // Afficher les points Arbo+Riegl

        if (d.index < 125){
            d.x = 850;
            d.y = y(d.index)*1.25;
        }else{
            d.x = 200;
            d.y = (y(d.index)-925)*2;
        };
        return d;        
    }))
    .enter()
    .append("circle")
    .attr("id",d => "node-" + d.index)
    .attr("r", "3.5")
    .attr("fill", "black")
    .attr("cx", (d)=> d.x)
    .attr("cy", (d)=> d.y);

// cree un label pour le noeud "node"
function create_label(node){
    const label = d3.select("#tore")
          .append("div")
          .attr("class", "node-label")
          .style('position', 'absolute')
          .style("top", node.y + 40 + "px")
          .html(node.attribut);

    if (node.index < 125){
        label.style("left", node.x + 40 + "px");
    }else{
        label.style("right", width-50+ "px");
    }
}

var panel = d3.select(document.getElementById("riegl")).append("div")
    .style("text-align", "center")
    .style("position", "absolute");

var panelbis = d3.select(document.getElementById("arbo")).append("div")
    .style("text-align", "center")
    .style("position", "absolute");

    var old_select = undefined;

    // svg.selectAll("circle").on("mouseleave", function(d){
    //     links_g.selectAll("line").style("opacity", 1);
    //     old_select = undefined;
    // });

    svg.selectAll("circle").on("mouseover", function(d) { // Afficher les attributs

        // suppression de tous les labels
        d3.selectAll(".node-label").remove();
        d3.selectAll("circle").transition().duration(200).style("fill", "black");

        // label du noeud survolé
        create_label(d);
        d3.select("#node-" + d.index).transition().duration(500).style("fill", "red");

        // label des noeuds liés
        d.connected.forEach(c => {
            create_label(c);
            d3.select("#node-" + c.index).transition().duration(500).style("fill", "red");
        });

        // liens
        links_g.selectAll("line").style("stroke", "gray").style("opacity", 0);
        if (old_select){
            old_select
                .transition()
                .duration(300)
                .style("stroke", "gray")
                .style("opacity", 0.0);
            panel.style("opacity", 0);
            panelbis.style("opacity", 0);

        }
        let sel = links_g.selectAll(".source-" + d.index);
        sel.transition()
            .duration(300)
            .style("stroke", "red")
            .style("opacity", 1);

        old_select = sel;
});
    
svg.selectAll("circle").on("click", function(d){
    console.log(d.x);
    console.log(d.y);
});
    

    var lst_links = [];
    data.links.forEach(l => {
        l.target.forEach(t => {
            let node_source = data.nodes[l.source];
            let node_target = data.nodes[t];
            let new_link = {
                source: data.nodes[l.source],
                target: data.nodes[t]
            };


            node_source.links.push(new_link);
            node_target.links.push(new_link);
            lst_links.push(new_link);

            if (node_source.connected.indexOf(node_target) < 0){
                node_source.connected.push(node_target);
            }
            if (node_target.connected.indexOf(node_source) < 0){
                node_target.connected.push(node_source);
            }
        });
    });

    console.log(lst_links);
var lienR = links_g.selectAll("line")
    .data(lst_links)
    .enter()
    .append("line")
    .attr("class", d => "source-" + d.source.index + " source-" + d.target.index)
    .attr("x1", (d)=> d.source.x) // cannot read property 'x' of undefined // je veux le x de l'élément-source
    .attr("y1", (d)=> d.source.y) 
    .attr("x2", (d)=> d.target.x) // idem, je veux le x de l'élément compris dans l'array target
    .attr("y2", (d)=> d.target.y)
    .attr("stroke", "gray")
    .attr("opacity", 0.0);
//    .attr("dx", (d)=> d.attribut.x)

// var lienR = svg.selectAll("line")
//     .data(data.nodes.filter(function(d){return d.targt != undefined;}).map(function(d) {
//         if (d.target != undefined){
//             console.log(d.attribut);
//             console.log(d.target);
            
//             var connexion = d.target;
//             var renvoi = data.nodes;
//             for (var i = 0; i < connexion.length; i++) { // pour chaque data dans l'array "target"
//                 for (var j = 0; j < renvoi.length; j++) { // pour chaque data dans l'array de base
//                     if(connexion[i] == renvoi[j].index) { // si la data de "target" correspond à un numéro d'index de l'array de base
//                         console.log(renvoi[j].attribut);
//                         var match = renvoi[j];
//                     }
//                 }
//             }
//         }
            
//             // exploiter d.target qui est un Array
//             // récupérer chaque donnée de l'Array
//             // renvoyer chaque donnée au numéro d'index
//             // récupérer les positions X et Y pour chacun de ces numéros d'index
//             // renvoyer les positions

//         d.match.x = 200;
//         d.match.y = y(match.index)-850;   
//         return match;
//     }))
//     .enter()
//     .append("line")
//     .attr("x1", (d)=> d.x) // cannot read property 'x' of undefined // je veux le x de l'élément-source
//     .attr("y1", (d)=> d.y) 
//     .attr("x2", (d)=> match.x) // idem, je veux le x de l'élément compris dans l'array target
//     .attr("y2", (d)=> match.y)
//     .attr("stroke", "gray")
//     .attr("opacity", 1);
// //    .attr("dx", (d)=> d.attribut.x)
// //    .attr("dy", (d)=> d.attribut.y);

});
