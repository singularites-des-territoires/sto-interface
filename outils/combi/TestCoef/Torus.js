// Source : https://observablehq.com/@renatoppl/torus-knots
// Fonction drag retirée

  var width = 640;
  var height = 210;
    
// GetTorus
    
    // GetTorus : k = nombre d'intersections entre les cercles (verticaux et horizontaux) Attention, la valeur prise en compte pour k est celle tout en bas du script !

function GetTorus(svg, x, y, scale, alpha = 0.5, k = 20) {
  var torus = svg.append("g");
  
  var segments = [];
  for (var i = 0; i < k; ++i) {
    for (var j = 0; j < k; ++j) {
      segments.push([[i/k,j/k],[(i+1)/k,j/k]]);
      segments.push([[i/k,j/k],[i/k,(j+1)/k]]);
    }
  }
  
  function SquareToTorus(p) {
    var z1 = p[0] * 2 * Math.PI;
    var z2 = p[1] * 2 * Math.PI;
    var t = 1 + alpha * Math.cos(z2); // échelle du cercle horizontal + grande pour avoir un creux au centre du tore
    return [Math.cos(z1) * t, Math.sin(z1) * t, alpha * Math.sin(z2)]; // cosZ2*cosZ1, cosZ2*sinZ1, alpha*cosZ2, alpha*sinZ2 // alpha = scale ?
  }

  function Rotate45yz(p) {
    var c = Math.sqrt(2)/2; // racine carrée de 2 = diagonale d'un carré de 1*1 (les cercles font 1 de diamètre?) // on divise par 2 pour avoir le rayon (moitié de diagonale)
    return [p[0], c * (p[1] + p[2]), c * (p[1] - p[2])]; // applique des déformations..? p[2] => axe des z (avant-arrière)
  }

  function Translate(p, dp) {
    return [p[0] + dp[0], p[1] + dp[1], p[2] + dp[2]]; // dp[2] => profondeur (axe avant-arrière)
  }

  function Project(p) {
    return [p[0] / (1-p[2]), p[1] / (1-p[2]), 1 / (1-p[2])]; // enlever p[2] pour ramener au même plan de projection que p[2] et p[1] ?
  }


  function Scale(p, s) {
    return [ p[0] * s, p[1] * s, p[2] * s];
  }

  function PreComposite(p) {
    return Translate(
      Scale(
        Project(
          Translate( // deux fois Translate ? Nécessaire pour que rotate s'effectue ?
            Rotate45yz(p), [0,0,-4]) // pourquoi -4 ? parcequ'on a scale + project + translate + rotate = 4 ?
        ), scale // pourquoi re scale ? (sans maj !?) => écriture de la fonction Scale(XX, scale) ?
      ), [x,y,0]
    );
  }

  function Composite(p) {
    return PreComposite( SquareToTorus(p) );
  }

  function CompositeSegment(s) {
    return s.map(Composite);
  }

  segments = segments.map(CompositeSegment);
    
// Mettre un filtre d'opacité selon la profondeur du point
// Pas compris syntaxe "...segments" / s[0][2]
  
  var maxz = Math.max(...segments.map(function(s) {return Math.max(s[0][2], s[1][2]);}));
  var minz = Math.min(...segments.map(function(s) {return Math.min(s[0][2], s[1][2]);}));

  function colortorbg(c, b = [1,1,1]) {
    c = 250 - 200 * (c-minz) / (maxz-minz);
    return d3.rgb(c * b[0],c * b[1],c * b[2]);
  }
  
  function AppendColor(basecolor, width) {
    return function(s) {
       var depth = (s[0][2] + s[1][2])/2;
       return [s[0], s[1], depth, colortorbg(depth, basecolor), width];
    }
  }
  
  segments = segments.map(AppendColor([1,1,1], 1));

  function Redraw() {
  segments = segments.sort(function(a,b) {return a[2] - b[2]}); // dessiner les premières lignes "a" après les "b" (lignes désopacifiées pour montrer la profondeur)
      
torus.selectAll("lines").remove();
    segments.forEach(function(s) {
      torus.append("line")
      .attr("stroke", s[3] )
      .attr("stroke-width", s[4])
      .attr("x1", s[0][0])
      .attr("y1", s[0][1])
      .attr("x2", s[1][0])
      .attr("y2", s[1][1]);
    });
  }
  
  Redraw();
  
  function DrawLine(ss, colorbase) {
    ss = ss.map(CompositeSegment);
    ss = ss.map(AppendColor(colorbase, 2));
    segments = ss.concat(segments);
    Redraw();
  }

  function GetTorusPoint() {
    var tp = torus.append("circle")
    .attr("r", 3.5);

    function UpdateTorusPoint(z1, z2) {
      //var z1 = c1.GetPoint();
      //var z2 = c2.GetPoint();
      var t = 1 + alpha * z2[0];
      var tpp = [z1[0] * t, z1[1] * t, alpha * z2[1]];
      tpp = PreComposite(tpp);
      tp.attr("cx", tpp[0])
        .attr("cy", tpp[1])
        .attr("fill", colortorbg(tpp[2], [1,0,0]));
    }

    return {
      UpdateTorusPoint : UpdateTorusPoint,
    }
  }

  return {
    GetTorusPoint : GetTorusPoint,
    DrawLine : DrawLine,
  }
}

//GetSquareInput

  function GetSquareInput(x, y, r, svg, callback) {
    var a = svg.append("g");

    var square = [[x,y], [x+r,y], [x+r,y+r], [x,y+r], [x,y]];
/*    for (var i = 0; i < 4; ++i) {
      a.append("line")
        .attr("stroke", "black")
        .attr("stroke-width", 1.5)
        .attr("x1", square[i][0])
        .attr("y1", square[i][1])
        .attr("x2", square[i+1][0])
        .attr("y2", square[i+1][1]);
    }*/ //Redondant (lignes noires en plus de celles en couleur plus bas)
    
    var xp = 0.5;
    var yp = 0.5;
      
      
    var w = 140,
    h = 140,
    xbase = 120,
    ybase = 30;
      
    d3.json("/outils/combi/TestCoef/CombiGold.json").then(function (data) {

    console.log(data);
        
    var ptRiegl = svg.selectAll("circle")
        .data(data.nodes.map(function (d) {
            
            //Test 1 pour afficher les points en fonction des coordonnées
            
/*            data.forEach(("circle") function(d) {
            d.x = xbase + w * Math.random();
            d.y = ybase + h * Math.random();
            return {
                x: d.x,
                y: d.y,
                label: d.attribut,
            };
            });*/
            
            //Test 2 pour afficher les points en fonction des coordonnées
            
/*            for(i=0, i<=124, i++) {
                d.x = xbase + w * Math.random();
                d.y = ybase + h * Math.random();
                return {
                    x: d.x,
                    y: d.y,
                    label: d.attribut,
                };
            }*/
            
            // Test 3 pour afficher les points en fonction des coordonnées
            // Aller chercher le numéro d'index dans le json => comment ?
            // Attribuer + 1
            // .style("left", position initiale + 1/(index+1)*largeur du svg + "px"))
                
                
            return d;
            
        })) // Pourquoi je n'ai qu'un seul point ?
        .enter()
        .append("circle")
        .attr("r", "3.5")
        .attr("fill", "black")
        .attr("cx", (d)=> xbase + 1 / d.index * w + "px") // renvoie la position du point ?
        .attr("cy", (d)=> ybase + 1 / d.index * h + "px") // renvoie la position du point ?
        .style("z-index", 99);
    
    });
      
    var ac = a.append("circle")
        .attr("r", 3.5)
        .attr("cx", x + r / 2)
        .attr("cy", y + r / 2)
        .call(d3.drag()
            .on("drag", dragged)); // supprimer le drag !

    function dragged() { // à remplacer par le placement (initialisation) des points de l'arbo/riegl + rajouter les dynamiques dans une nouvelle fonction
      var px = Math.max(0, Math.min(r, d3.event.x - x));
      var py = Math.max(0, Math.min(r, d3.event.y - y));
      
      xp = px / r;
      yp = py / r;
      
      ac.attr("cx", x + r * xp)
        .attr("cy", y + r * yp);
      
      callback();
    }
    
    function Rescale(p) {
      return [x + r * p[0], y + r * p[1]];
    }
    
    function DrawLine(p1, p2, color) {
      p1 = Rescale(p1);
      p2 = Rescale(p2);
      a.append("line")
        .attr("stroke", color)
        .attr("stroke-width", 2.5)
        .attr("x1", p1[0])
        .attr("y1", p1[1])
        .attr("x2", p2[0])
        .attr("y2", p2[1]);
    }
    
/*    function DisableDragging() {
      ac.call(d3.drag()
        .on("drag", function() {}));
    }*/
    
    function UpdatePointPosition(xn, yn) {
      xp = xn;
      yp = yn;
      ac.attr("cx", x + r * xp)
        .attr("cy", y + r * yp);
      callback();
    }
    
    function SetPointMotion(f) {
      var pn = f([xp,yp]);
      UpdatePointPosition(pn[0], pn[1]);
      setTimeout(function() { SetPointMotion(f); }, 20);
    }
    
    return {
      GetPoint : function() { return [xp * 2 * Math.PI, yp * 2 * Math.PI]; },
      DrawLine : DrawLine,
//      DisableaDragging : DisableDragging,
      UpdatePointPosition : UpdatePointPosition,
      SetPointMotion : SetPointMotion,
    }
  }
    
//Torus + square

  var svg = d3.select(document.getElementById("tore")).append("svg")
    .attr("width", width)
    .attr("height", height)
    .style("overflow", "visible");
 
  var s = GetSquareInput(120,30,140, svg, TorusCallback) 
  s.DrawLine([0,0],[0,1], "blue");
  s.DrawLine([1,0],[1,1], "blue");
  s.DrawLine([0,0],[1,0], "green");
  s.DrawLine([0,1],[1,1], "green");

  svg.append("text")
    .attr("x", 290)
    .attr("y", 105)
    .style("font-size", "34px")
    .text("≃");

  var torus = GetTorus(svg, 440, 90, 350, 0.5, 20);
  var tp = torus.GetTorusPoint();
  
  var bluesegments = []; //Riegl
  var greensegments = []; //Arbo
  var k = 20;
  for (var j = 0; j < k; ++j) {
    bluesegments.push([[0,j/k],[0,(j+1)/k]]); //Riegl
    greensegments.push([[j/k,0],[(j+1)/k,0]]); //Arbo
  }
  torus.DrawLine(bluesegments, [0,0,1]); //Riegl
  torus.DrawLine(greensegments, [0,1,0]); // Arbo

  
  function TorusCallback() {
    var xy = s.GetPoint(); // dépend des points de GetSquareInput
    tp.UpdateTorusPoint([Math.cos(xy[0]), Math.sin(xy[0])],
                        [Math.cos(xy[1]), Math.sin(xy[1])]);
  }
  
  TorusCallback();

//  return svg.node();
