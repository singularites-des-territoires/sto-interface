
function create_timeline(parent, data){
    // sélection du parent
    let container = d3.select(parent);

    let margin = 40;

    // récupération de la taille du parent pour dimensionner le svg
    let bb = container.node().getBoundingClientRect();
    let width = bb.width;
    let height = bb.height;

    let date_min = new Date("1900");
    let date_max = new Date("2020");

    // création de la timeline svg
    const svg = container.append("svg")
        .style("width", width + "px")
        .style("height", height + "px");

    // autres éléments html
    const tip = container.append("div")
          .attr("class", "tl-overlay");
    let w_tip = tip.node().getBoundingClientRect().width;

    // échelle horizontale
    let x_scale = d3.scaleLinear()
        .domain([date_min.getFullYear(), date_max.getFullYear()])
        .range([margin, width-margin]);
        // .nice();
        // .interpolate(d3.interpolateRound);

    // axe horizontal
    let x_axis = d3.axisBottom(x_scale);
        // .tickFormat(d3.timeFormat("%Y"));

    // création du svg pour l'axe x
    const x_axis_g = svg.append("g")
          .attr("transform", `translate(0, ${ height/2})`);
    x_axis_g.call(x_axis);

    const data_g = svg.append("g");

    // chargement des données
    d3.json(data).then( data =>{
        let cpt = 0;
        for (const date of data){
            date.id = cpt;
            date.t = new Date(date.date);
            date.y = height/4;
            date.year = date.t.getFullYear(); 
            cpt ++;
        }
        update(data);
    });

    function show_tip(d){
        tip.html("");
        tip.append("h2").html(d.titre);
        tip.append("h3").html(d.date);
        tip.append("p").html(d.description);
        tip.style("left", x_scale(d.year) - w_tip/2 + "px")
            .style("top", d.y + 25 + "px");
        tip.transition().style("opacity", 1);
    }

    function hide_tip(d){
        tip.interrupt().transition().style("opacity", 0);
        // tip.style("opacity", 0);
    }

    function update(data){
        console.log("update");
        data_g.selectAll("g")
            .data(data, d => d.id)
            .join(
                enter => {
                    const g = enter.append("g");
                    g.attr("transform", d => `translate(${ x_scale(d.year)}, ${ d.y })`);

                    g.append("line")
                        .style("stroke", "gray")
                        .attr("x1", 0)
                        .attr("y1", -height)
                        .attr("x2", 0)
                        .attr("y2", height);

                    g.append("circle")
                        .attr("cx", 0)
                        .attr("cy", 0)
                        .attr("r", 10);


                    g.on("mouseover", (e, d) => {
                        show_tip(d);
                    });

                    g.on("mouseleave", (e, d) => {
                        hide_tip(d);
                    });
                },
                update => update.attr("transform", d => `translate(${ x_scale(d.t)}, ${ d.y })`)
            );


    }
}
