var w = 900,
    h = 600;

var svg = d3.select(document.getElementById("frise-cadre")).append("svg")
    .attr("width", w)
    .attr("height", h)
    .style("margin-left", 100)
    .style("border", "groove medium");

var panel = d3.select(document.getElementById("frise")).append("div")
    .style("left", 0)
    .style("text-align", "center")
    .style("position", "relative");

// Les panel BIS sont inutiles à partir du moment où on arrive à faire un saut de ligne

var panelbis = d3.select(document.getElementById("frise")).append("div")
    .style("left", 0)
    .style("text-align", "center")
    .style("font-weight", "900")
    .style("position", "relative")
    .style("margin-bottom", "5px");

var panel1 = d3.select(document.getElementById("frise1")).append("div")
    .style("left", 0)
    .style("text-align", "center")
    .style("position", "relative");

var panel1bis = d3.select(document.getElementById("frise1")).append("div")
    .style("left", 0)
    .style("text-align", "center")
    .style("font-weight", "900")
    .style("position", "relative");

var miniature = d3.select(document.getElementById("image")).append("div")
    .style("position", "relative")
    .style("height", "120px")
    .style("width", "120px")
    .style("border-radius", "60px")
    .style("left", 0)
    .style("top", 0);

miniature.append("img");

var radius = 6;

d3.csv("/outils/dots/data.csv").then(function (data) {

    console.log(data);

    // d => { return d.lien;};
    // function(d){return d.lien;};

//    data.forEach(d => d.lien = d.date  + " " + d.ville);
    
    var line_h = svg.append("line")
        .attr("x1", 0)
        .attr("y1", 0)
        .attr("x2", w)
        .attr("y2", 0);

    var line = svg.append("line")
        .attr("x1", 0)
        .attr("y1", 0)
        .attr("x2", 0)
        .attr("y2", h);
    
    var circle = svg.selectAll("circle")
        .data(data.map(function(d) {
            return {
                x: w * Math.random(),
                y: h * Math.random(),
                dx: 1/2*Math.random() - 0.5,
                dy: 1/2*Math.random() - 0.5,
                ville: d.ville,
                date: d.date,
                lien: d.lien,
                longitude: d.longitude,
                latitude: d.latitude,
                url: d.url,
            };
        }))
        .enter()
        .append("circle")
        .attr("r", radius)
        .attr("fill", "black")
        .style("z-index", 99);
    
//    svg.selectAll("circle").data(data, function(d) {
//        if (d.longitude =! "Modèle") {
//            circle.attr("fill", "black");
//        }
//        else {
//            circle.attr("fill", "gray");
//        }
//    });

    var sel_circle = undefined;

    svg.selectAll("circle").on("mouseover", function(d){
        sel_circle = d;
//        console.log(d.ville);
        line.attr("stroke", "gray")
            .attr("opacity", 1);
        line_h.attr("stroke", "gray")
            .attr("opacity", 1);
        panel.html(d.latitude + '<br>' + '<strong>' + d.date + '</strong>')
            .style("left", 50 + d.x + "px");
        panel1.html(d.longitude + '<br>' + '<strong>' + d.ville + '</strong>')
            .style("top", -15 + d.y + "px");
        
//        Ce qui suit empêche click de se réaliser
        miniature.style("display", "block")
//        petit rond
            .style("z_index", 99)
            .style("left", 100 + d.x + "px")
            .style("top", d.y + "px")
            .style("border", "groove medium")
            .style("background-color", "white")
            .style("background-image", d.url)
            .style("background-size", "cover")
            .style("background-position", "center")
            .style("height", "8px")
            .style("width", "8px");
        miniature.transition()
//        rond moyen
            .style("height", "80px")
            .style("width", "80px")
            .style("border-radius", "60px")
            .style("left", 60 + d.x + "px")
            .style("top", d.y - 40 + "px");
    });
    
    var sel_circle1 = undefined;
    
    svg.selectAll("img").on("mouseout", function() {
//        Ne fonctionne plus
//        panel.attr("opacity", 0);
        line.attr("opacity", 0);
        line_h.attr("opacity", 0);
        sel_circle1 = undefined;
        sel_circle = undefined;
    });

     svg.on("click", function() {
            console.log(sel_circle1);

    //Cliquer sur le point pour afficher l'image
    // Cliquer ailleurs pour masquer l'image
            if (sel_circle1 == undefined) {
                miniature.style("display", "none");
            };
            sel_circle1 = undefined;
        });

    miniature.on("click", function(d) {
        sel_circle1 = 1;
        
        if (sel_circle1 = 1) {
            
            sel_circle1 = undefined;
            miniature.transition()
//            rectangle
                .style("height", "120px")
                .style("width", "250px")
                .style("border-radius", "0px");
//            PROPIETES suivantes NON LIEES A L'ELEMENT miniature
//                .style("left", d.x - 25 + "px")
//                .style("top", d.y - 60 + "px");
        }
    });
    
    
    d3.timer(function() {
        circle
            .filter(d => d != sel_circle)
            .attr("cx", function(d) {
                d.x += d.dx;
                if (d.x > w - radius) {
                    d.dx = -d.dx;
                    d.x = w - radius;
                }
                else if (d.x < radius) {
                    d.x = radius;
                    d.dx = -d.dx;
                }
                return d.x;
            })
            .attr("cy", function(d) { 
                d.y += d.dy;
                if (d.y > h - radius) {
                    d.dy = -d.dy;
                    d.y = h - radius;
                } 
                else if (d.y < radius) {
                    d.y = radius;
                    d.dy = -d.dy;
                } 
                return d.y;
            });

        if (sel_circle){
            line_h.attr("y1", sel_circle.y)
                .attr("y2", sel_circle.y);

            line.attr("x1", sel_circle.x)
                .attr("x2", sel_circle.x);
                        
        };
        
    });
});

