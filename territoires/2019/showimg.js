var lst_blocks;

$(document).ready(function(){
    lst_blocks = $('.js-cd-content');
    
    lst_blocks.each(function(d){
	var liens = $(this).find('a');

	var block = $(this);

	if (liens.length > 0){
	    liens.on('click', function(){	
		if (block.hasClass('cd-showimg') == false){
		    block.addClass('cd-showimg');
		    block.find('.cd-timeline-before').hide(500);
		    block.find('.cd-timeline-after').show(500);
		}else{
		    console.log('hasclass');
		    block.removeClass('cd-showimg');
		    block.find('.cd-timeline-after').hide(500);
		    block.find('.cd-timeline-before').show(500);
		}
	    });
	    
	};
	// $(this).on('click', function(){
	//     if ($(this).hasClass('cd-showimg')){
	// 	console.log('click');
	// 	$(this).find('.cd-timeline-after').hide(500);
	// 	$(this).find('.cd-timeline-before').show(500);
	//     }
	// });
    })
    
});
