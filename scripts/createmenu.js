//crée le menu dans la div menu, var deroulant est l'arborescence du site

$(document).ready(function () {
    var is_accueil = $('body').hasClass('accueil');
    var is_archives = $('body').hasClass('archives');

    console.log(is_accueil);
    var deroulant = [
        {
            'titre': 'OUTILS',
            'class': 'menu',
            'children': [{
                    'titre': 'Présentation',
                    'lien': 'presentation.html',
                    'class': 'sousmenu',

		},
                {
                    'titre': 'Arborescence',
                    'lien': 'arbo.html',
                    'class': 'sousmenu',

		},
                {
                    'titre': 'Riegl',
                    'lien': 'index.html',
                    'class': 'sousmenu',

		},
                {
                    'titre': 'Bibliographie',
                    'lien': 'bibliographie.html',
                    'class': 'sousmenu',

		}]
	}, {
            'titre': 'TERRITOIRES',
            'class': 'menu',
            'children': [{
                    'titre': 'Urbino',
                    'class': 'sous-menu-déroulant',
                    'children': [{
                        'titre': '2009',
                        'class': 'sousmenu',
                        'lien': '2009_index.html'
    }, {
                        'titre': '2010',
                        'class': 'sousmenu',
                        'lien': '2010_index.html'
    }, {
                        'titre': '2011',
                        'class': 'sousmenu',
                        'lien': '2011_index.html',

    }],

    }, {
                    'titre': 'Lisbonne',
                    'class': 'sous-menu-déroulant',
                    'children': [{
                        'titre': '2012',
                        'class': 'sousmenu',
                        'lien': '2012_index.html',
                        
    }, {
                        'titre': '2013',
                        'class': 'sousmenu',
                        'lien': '2013_index.html',
                       
    }, {
                        'titre': '2014',
                        'class': 'sous-sous-menu',
                        'lien': '2014_index.html',
                       
    }, {
                        'titre': '2015',
                        'class': 'sous-sous-menu',
                        'lien': '2015_index.html',
    }],
                },
                {
                    'titre': 'Athènes',
                    'lien': 'TabulationsLien',
                    'class': 'sous-menu-déroulant',
                    'children': [{
                        'titre': '2016',
                        'class': 'sous-sous-menu',
                        'lien': '2016_index.html',
                        
    }, {
                        'titre': '2017',
                        'class': 'sous-sous-menu',
                        'lien': '2017_index.html',
                        
    }, {
                        'titre': '2018',
                        'class': 'sous-sous-menu',
                        'lien': '2018_index.html',
                           }]

                            }]
                    }];


    deroulant.forEach(function (e) { //permet de changer les liens selon index ou pas
        var rep = "";
        if (is_accueil == true) {
            rep = "html/";
        }
        if (is_archives == true) {
            rep = "../../../../html/"
        }

        var nav = $('<li class="toggleSubMenu"><span>' + e['titre'] + '</span></li>').appendTo($('#navig'));
        console.log(nav);

//Niveau 1 du menu
        var nav_ul = $('<ul class="subMenu"> </ul>').appendTo(nav);
        for (var i in e.children) { //dans les enfants de menu
            var c = e.children[i]; //c prend la valeur des sousmenu
            if (!c.children) { // si il existe pas de sous-sous-menu
                nav_ul.append('<li><a class="sous" href="' + rep + c['lien'] + '">' + c['titre'] + '</a></li>');
            } else { // sinon
//Niveau 2 du menu
                var nav2 = $('<li class="toggleSubSubMenu"><span>' + c['titre'] + '</span></li>').appendTo(nav_ul);
                var nav_ul2 = $('<ul class="sousmenu"> </ul>').appendTo(nav2);
                for (var j in c.children) {
                    var f = c.children[j]
                    var nav3 = $('<li><a class="sous" href="' + rep + f['lien'] + '">' + f['titre'] + '</a></li>').appendTo(nav_ul2);
                    var nav_ul3 = $('<ul class="sous"> </ul>').appendTo(nav3);
                 	for (var k in f.children) {
                        nav_ul3.append('<li><a class="sous" href="' + rep + f.children[k]['lien'] + '">' + f.children[k]['titre'] + '</a></li>');
                    }
                   
                }
            }
        }






    });

	
	
//crée le titre de la page dans la div head avec une fonction if qui permet de changer les liens selon index ou pas

    console.log(deroulant);

    var repe = "../";
    if (is_accueil == true) {
        repe = "";
    }
    if (is_archives == true) {
        repe = "../../../../"
    }

    var header = $('<strong><a href="' + repe + 'index.html">Singularités des territoires </a></strong><a href ="http://morpholab.fr/fr/home/"> | MorphoLab</a><a href ="http://www.nantes.archi.fr/"> | ENSAN</a>').appendTo($('#head'))

	
	
//crée le footer dans la div footer avec une fonction if qui permet de changer les liens selon index ou pas 

    var reper = "";
    if (is_accueil == true) {
        reper = "html/";
    }
    if (is_archives == true) {
        reper = "../../../../html/"
    }

    var footer = $('<table><tr><td><a class="floatleft" href="https://www.facebook.com/groups/345637762171186/"><img src="' + repe + 'images/footer/fb.png"></a><br><a class="floatleft" href="https://twitter.com/morpho_lab"><img src="' + repe + 'images/footer/twitter.png"></a></td><td class="floatright">©SingularitésDesTerritoires2018</td></tr></table>').appendTo($('#footer'))

	
	
//bouton qui ouvre le menu /!\ ce script doit être toujours après celui qui crée l'html du menu sinon ça ne marchera pas !

    // On cache les sous-menus
    $(".navigation ul.subMenu").hide();
    // On sélectionne tous les items de liste portant la classe "toggleSubMenu"

    // et on remplace l'élément span qu'ils contiennent par un lien :
    $(".navigation li.toggleSubMenu span").each(function () {
        // On stocke le contenu du span :
        var TexteSpan = $(this).text();
        $(this).replaceWith('<a href="" title="Afficher le sous-menu">' + TexteSpan + '<\/a>');
    });

    $(".navigation ul.subsubMenu").hide();
    // On sélectionne tous les items de liste portant la classe "toggleSubMenu"

    // et on remplace l'élément span qu'ils contiennent par un lien :
    $(".navigation li.toggleSubSubMenu span").each(function () {
        // On stocke le contenu du span :
        var TexteSpan = $(this).text();
        $(this).replaceWith('<a href="" title="Afficher le sous-menu">' + TexteSpan + '<\/a>');
    });
    
    $(".navigation ul.subsubsubMenu").hide();
    // On sélectionne tous les items de liste portant la classe "toggleSubMenu"

    // et on remplace l'élément span qu'ils contiennent par un lien :
    $(".navigation li.toggleSubSubSubMenu span").each(function () {
        // On stocke le contenu du span :
        var TexteSpan = $(this).text();
        $(this).replaceWith('<a href="" title="Afficher le sous-menu">' + TexteSpan + '<\/a>');
    });

    // On modifie l'évènement "click" sur les liens dans les items de liste
    // qui portent la classe "toggleSubMenu" :
    $(".navigation li.toggleSubMenu > a").click(function () {
        // Si le sous-menu était déjà ouvert, on le referme :
        if ($(this).next("ul.subMenu:visible").length != 0) {
            $(this).next("ul.subMenu").slideUp("normal", function () {
                $(this).parent().removeClass("open")
            });
        }
        // Si le sous-menu est caché, on ferme les autres et on l'affiche :
        else {
            $(".navigation ul.subMenu").slideUp("normal", function () {
                $(this).parent().removeClass("open")
            });
            $(this).next("ul.subMenu").slideDown("normal", function () {
                $(this).parent().addClass("open")
            });
        }
        // On empêche le navigateur de suivre le lien :
        return false;
    });

    $(".navigation li.toggleSubSubMenu > a").click(function () {
        // Si le sous-menu était déjà ouvert, on le referme :
        if ($(this).next("ul.subsubMenu:visible").length != 0) {
            $(this).next("ul.subsubMenu").slideUp("normal", function () {
                $(this).parent().removeClass("open2")
            });
        }
        // Si le sous-menu est caché, on ferme les autres et on l'affiche :
        else {
            $(".navigation ul.subsubMenu").slideUp("normal", function () {
                $(this).parent().removeClass("open2")
            });
            $(this).next("ul.subsubMenu").slideDown("normal", function () {
                $(this).parent().addClass("open2")
            });
        }
        // On empêche le navigateur de suivre le lien :
        return false;
    });
    
    $(".navigation li.toggleSubSubSubMenu > a").click(function () {
        // Si le sous-menu était déjà ouvert, on le referme :
        if ($(this).next("ul.subsubsubMenu:visible").length != 0) {
            $(this).next("ul.subsubsubMenu").slideUp("normal", function () {
                $(this).parent().removeClass("open3")
            });
        }
        // Si le sous-menu est caché, on ferme les autres et on l'affiche :
        else {
            $(".navigation ul.subsubsubMenu").slideUp("normal", function () {
                $(this).parent().removeClass("open3")
            });
            $(this).next("ul.subsubsubMenu").slideDown("normal", function () {
                $(this).parent().addClass("open3")
            });
        }
        // On empêche le navigateur de suivre le lien :
        return false;
    });

   
    
    
    
    
    
    
    
    $(".close").css("display", "none");

    var isMenuOpen = false;
    var isNavOpen = true;
    var isNavFull = false;

    $('.menu_btn').click(function () {
        if (isMenuOpen == false) {
            //alert('je suis dans le bon cas')
            $("#menu").clearQueue().animate({
                left: '0'
            })
            $("#page").clearQueue().animate({
                "margin-right": '-400px'
            })

            //                    $(this).fadeOut(200);
            $(".close").fadeIn(300);

            isMenuOpen = true;
        } else {
            $("#menu").clearQueue().animate({
                left: '-400px'
            })
            $("#page").clearQueue().animate({
                "margin-right": '0px'
            })

            $(this).fadeOut(200);
            $(".menu_btn").fadeIn(300);

            isMenuOpen = false;
        }
    });


});
